#include "SDK\\sdk.h"

#include "menu.h"

DWORD PTR_EXPORTS = 0;
#define ENGFUNCS 0
#define ENGSTUDIO 0


ExportTable_s *pExport;
ExportTable_s oExport = {0};

CVAR cvars;

cl_enginefuncs_s		*pEngfuncs;
engine_studio_api_s		*pEngstudio;
DWORD SOUND = 0;

cl_enginefuncs_s		oEngfuncs	= { 0 };
engine_studio_api_s		oEngstudio	= { 0 };

SCREENINFO screeninfo;




void coderinfo()
{
	pEngfuncs->pfnConsolePrint("Gecoded am 25. Februar 2009 von Gordon`\n");
}


extern int counter;
int HUD_Redraw(float flTime, int intermission)
{
	static bool bInit = false;
	if(!bInit)
	{
		//CreateThread(0, 0, (LPTHREAD_START_ROUTINE)WriteBackup, 0, 0, 0);
		
		screeninfo.iSize=sizeof(SCREENINFO);
		pEngfuncs->pfnGetScreenInfo(&screeninfo);
		
		pEngfuncs->pfnAddCommand("coderinfo", coderinfo);
		
		SetupUserMessageHooks();

		bInit = true;
	}

	
	cvars.Check = 1;
	
	if(GetAsyncKeyState(VK_F12) & 1)
	{
		if(cvars.Panickey)
			cvars.Panickey = false;
		else if(!cvars.Panickey)
			cvars.Panickey = true;
	}

	if(!cvars.Panickey)
	{	
		for(int i = 1; i < 33; i++)
		{
			gPlayers[i]->bGotHead = false; 
			esp->Main(i);
		}


	}

	return oExport.HUD_Redraw(flTime, intermission);
}

bool bMenuKeyToggle;
int HUD_Key_Event(int eventcode, int keynum, const char *pszCurrentBinding)
{

	return oExport.HUD_Key_Event(eventcode, keynum, pszCurrentBinding);
}

float mainViewOrigin[3] = {0};
void CalcRefdef(struct ref_params_s *pparams)
{	
	for(int i = 0; i < 33; i++) 
	{
			gPlayers[i]->bGotHead = false; 
	}

	VectorCopy(pparams->vieworg,mainViewOrigin);

	return oExport.V_CalcRefdef(pparams);
}


#define COPY(x, y, z) { gPlayers[i]->fSoundOrigin[0] = x; gPlayers[i]->fSoundOrigin[1] = y; gPlayers[i]->fSoundOrigin[2] = z;}

void AtRoundStart()
{
	for(int i =0; i < 33; i++)
	{
		gPlayers[i]->bAlive = true;
		COPY(0,0,0);
		gPlayers[i]->bGotHead = false; 
	}
}


bool bSetupSuc = false;
void SetupClient() //neu
{
	if (bSetupSuc) return;

	if (!pExport->Initialize) return;
	memcpy(&oExport, pExport, sizeof(ExportTable_s));
	
	pExport->HUD_Redraw = &HUD_Redraw;
	pExport->V_CalcRefdef = &CalcRefdef;
	//pExport->HUD_Key_Event = &HUD_Key_Event;

	bSetupSuc = true;
}



bool bSetSuc = false;
void SetupEngine() //neu
{
	if (bSetSuc) return;

	if (!pEngfuncs || !pEngfuncs->pfnHookEvent) return;
	memcpy(&oEngfuncs, pEngfuncs, sizeof(cl_enginefuncs_s));

	if (!pEngstudio || !pEngstudio->GetModelByIndex) return;
	memcpy(&oEngstudio, pEngstudio, sizeof(engine_studio_api_s));

	bSetSuc = true;
}