#ifndef _CLIENT_
#define _CLIENT_

#include "SDK\\sdk.h"

extern void SetupClient();
extern void SetupEngine();

extern cl_enginefuncs_s	*pEngfuncs;
extern engine_studio_api_s		*pEngstudio;
extern double * globaltime;
extern playermove_t* playermove;
extern SCREENINFO screeninfo;
extern ExportTable_s *pExport;
extern ExportTable_s oExport;
extern DWORD SOUND;
extern int boxsize;

#endif