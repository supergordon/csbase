#include "SDK\\sdk.h"
#include "Other\\detours.h"

#include "menu.h"


typedef void (WINAPI *glPopMatrix_t) (void);
glPopMatrix_t pglPopMatrix = 0;


DWORD dwFunctionAddress = NULL;
HMODULE k_Module = NULL;
BYTE byOriginal[5];



using namespace std;

void *DetourFunc(BYTE *src, const BYTE *dst, const int len)
{
	BYTE *jmp = (BYTE*)malloc(len+5);
	DWORD dwback;

	VirtualProtect(src, len, PAGE_READWRITE, &dwback);

	memcpy(jmp, src, len);	jmp += len;
	
	jmp[0] = 0xE9;
	*(DWORD*)(jmp+1) = (DWORD)(src+len - jmp) - 5;

	src[0] = 0xE9;
	*(DWORD*)(src+1) = (DWORD)(dst - src) - 5;

	VirtualProtect(src, len, dwback, &dwback);

	return (jmp-len);
}

void VectorTransform (float *in1, float in2[3][4], float *out)
{
	out[0] = DotProduct(in1, in2[0]) + in2[0][3];
	out[1] = DotProduct(in1, in2[1]) + in2[1][3];
	out[2] = DotProduct(in1, in2[2]) + in2[2][3];
}

typedef float TransformMatrix[MAXSTUDIOBONES][3][4];
void CalculateHitbox(cl_entity_s *pEnt)
{
		if(!gPlayers[pEnt->index]->bGotHead )
		{
			int iIndex = pEnt->index;

			model_s		    *pModel			= pEngstudio->SetupPlayerModel( iIndex );
			studiohdr_t     *pStudioHeader	= ( studiohdr_t* )pEngstudio->Mod_Extradata( pModel );
			mstudiobbox_t   *pStudioBox;
			TransformMatrix *pBoneTransform = ( TransformMatrix* )pEngstudio->StudioGetBoneTransform();	

			vec3_t vMin, vMax;
			pStudioBox = ( mstudiobbox_t* )( ( byte* )pStudioHeader + pStudioHeader->hitboxindex );

			//Head 11 bone 7 | Low Head 9 bone 5 | Chest 8 bone 4 | Stomach 7 bone 3
			int i = 11;

			VectorTransform(pStudioBox[i].bbmin, (*pBoneTransform)[pStudioBox[i].bone], vMin);
			VectorTransform(pStudioBox[i].bbmax, (*pBoneTransform)[pStudioBox[i].bone], vMax);
			gPlayers[iIndex]->vHitbox	= ( vMin + vMax ) * 0.5f;
			gPlayers[iIndex]->bGotHead	= true;	
		}
}

void WINAPI hkPopMatrix(void)
{
	__asm pushad
	if(!cvars.Panickey)
	{
		cl_entity_t * pEnt; 
		if(pEngstudio)
		{
			pEnt = pEngstudio->GetCurrentEntity(); 
			if(pEnt && pEnt->player) 
				CalculateHitbox(pEnt);
		}
	}
	__asm popad
	
	return pglPopMatrix();
}

int counter = 0;
CVAR oldVars;


void Hooking()
{
   HANDLE lol = 0;
   while(! (lol = GetModuleHandle("hw.dll")))
	   Sleep(100);

   lol = 0;

   while(! (lol = GetModuleHandle("client.dll")))
	   Sleep(100);
    //

   HMODULE opengl = 0;
   while(!opengl)
   {
		opengl = GetModuleHandle("opengl32.dll");
		Sleep(100);
   }

	DWORD dwPopMatrix = (DWORD)GetProcAddress(opengl, "glPopMatrix");
	pglPopMatrix = (glPopMatrix_t)DetourFunc((PBYTE)dwPopMatrix, (PBYTE)hkPopMatrix, 6);
	
	
	InitPlayers();
	Sleep(2000);

	GetOffsets();
	SetupEngine();
	SetupClient();
}


BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
		case DLL_PROCESS_ATTACH:
		{
			CreateThread(0, 0, (LPTHREAD_START_ROUTINE)Hooking, 0, 0, 0);
			HideModule(hinstDLL);
			break;
		}
		case DLL_PROCESS_DETACH:
		{
			DeletePlayers();
			break;
		}
	}
	return true;
}

bool bGotOffsets = false;


void HideModule(HINSTANCE hModule)
{
	DWORD dwPEB_LDR_DATA = 0;
	_asm
	{
		pushad;
		pushfd;
		mov eax, fs:[30h]		   // PEB
		mov eax, [eax+0Ch]		  // PEB->ProcessModuleInfo
		mov dwPEB_LDR_DATA, eax	 // Save ProcessModuleInfo

InLoadOrderModuleList:
		mov esi, [eax+0Ch]					  // ProcessModuleInfo->InLoadOrderModuleList[FORWARD]
		mov edx, [eax+10h]					  //  ProcessModuleInfo->InLoadOrderModuleList[BACKWARD]

		LoopInLoadOrderModuleList: 
		    lodsd							   //  Load First Module
			mov esi, eax		    			//  ESI points to Next Module
			mov ecx, [eax+18h]		    		//  LDR_MODULE->BaseAddress
			cmp ecx, hModule		    		//  Is it Our Module ?
			jne SkipA		    		    	//  If Not, Next Please (@f jumps to nearest Unamed Lable @@:)
		    	mov ebx, [eax]				  //  [FORWARD] Module 
		    	mov ecx, [eax+4]    		    	//  [BACKWARD] Module
		    	mov [ecx], ebx				  //  Previous Module\'s [FORWARD] Notation, Points to us, Replace it with, Module++
		    	mov [ebx+4], ecx			    //  Next Modules, [BACKWARD] Notation, Points to us, Replace it with, Module--
			jmp InMemoryOrderModuleList		//  Hidden, so Move onto Next Set
		SkipA:
			cmp edx, esi					    //  Reached End of Modules ?
			jne LoopInLoadOrderModuleList		//  If Not, Re Loop

InMemoryOrderModuleList:
		mov eax, dwPEB_LDR_DATA		  //  PEB->ProcessModuleInfo
		mov esi, [eax+14h]			   //  ProcessModuleInfo->InMemoryOrderModuleList[START]
		mov edx, [eax+18h]			   //  ProcessModuleInfo->InMemoryOrderModuleList[FINISH]

		LoopInMemoryOrderModuleList: 
			lodsd
			mov esi, eax
			mov ecx, [eax+10h]
			cmp ecx, hModule
			jne SkipB
				mov ebx, [eax] 
				mov ecx, [eax+4]
				mov [ecx], ebx
				mov [ebx+4], ecx
				jmp InInitializationOrderModuleList
		SkipB:
			cmp edx, esi
			jne LoopInMemoryOrderModuleList

InInitializationOrderModuleList:
		mov eax, dwPEB_LDR_DATA				    //  PEB->ProcessModuleInfo
		mov esi, [eax+1Ch]						 //  ProcessModuleInfo->InInitializationOrderModuleList[START]
		mov edx, [eax+20h]						 //  ProcessModuleInfo->InInitializationOrderModuleList[FINISH]

		LoopInInitializationOrderModuleList: 
			lodsd
			mov esi, eax		
			mov ecx, [eax+08h]
			cmp ecx, hModule		
			jne SkipC
				mov ebx, [eax] 
				mov ecx, [eax+4]
				mov [ecx], ebx
				mov [ebx+4], ecx
				jmp Finished
		SkipC:
			cmp edx, esi
			jne LoopInInitializationOrderModuleList

		Finished:
			popfd;
			popad;
	}
}

bool bDataCompare(const BYTE* pData, const BYTE* bMask, const char* szMask)
{
	for(;*szMask;++szMask,++pData,++bMask)
		if(*szMask=='x' && *pData!=*bMask ) 
			return false;
	return (*szMask) == NULL;
}

DWORD dwFindPattern(DWORD dwAddress,DWORD dwLen,BYTE *bMask,char * szMask)
{
	for(DWORD i=0; i < dwLen; i++)
		if( bDataCompare( (BYTE*)( dwAddress+i ),bMask,szMask) )
			return (DWORD)(dwAddress+i);

	return 0;
}

void GetOffsets()
{	
	static bool bInit = false;
 
	if(bInit)
		return;
 
	
	DWORD dwHW	   = (DWORD)GetModuleHandle("hw.dll");
	DWORD dwHWSize = 0x122A000;
 
 
	/*BYTE Sound_SIG[] = "\x83\xEC\x00\xA1\x00\x00\x00\x00\x53\x55\x56\x85\xC0";
	CHAR Sound_MSK[] = "xx?x????xxxxx";
	SOUND = dwFindPattern(dwHW, dwHWSize, Sound_SIG, Sound_MSK);*/
 
 
	BYTE Export_SIG[] = "\x8B\x44\x24\x04\x6A\x00\x68\x00\x00\x00\x00\x68";
	CHAR Export_MSK[] = "xxxxxxx????x";
	DWORD dwExport = dwFindPattern(dwHW, dwHWSize, Export_SIG, Export_MSK) + 0x7;
	pExport = (ExportTable_s*)*(DWORD*)dwExport;
	//8B 44 24 04 6A 00 68 ?? ?? ?? ?? 68 +0x7*
 
 
	DWORD dwEngfuncs = (DWORD)*pExport->Initialize + 0x1C;
	pEngfuncs = (cl_enginefunc_t*)*(DWORD*)dwEngfuncs;
	//067DE1C0  0AEA4ED0  Initialize
	//$+1B     >|.  BF 901BF80A   MOV EDI,client.0AF81B90
 
 
	DWORD dwEngstudio = (DWORD)*pExport->HUD_GetStudioModelInterface + 0x1A;
	pEngstudio = (engine_studio_api_s*)*(DWORD*)dwEngstudio;
	//067DE25C  0AEAADB0  HUD_GetStudioModelInterface
	//$+19     >|.  BF 408DF90A   MOV EDI,client.0AF98D40



	
	pEngfuncs->pfnConsolePrint("Scanning Offsets...\n");
	//pEngfuncs->pfnClientCmd("toggleconsole");
	
	char bla[333] = "";
	sprintf(bla,"Enginefuncs: %X\nEnginestudio: %X\nExporttable: %X\n", pEngfuncs, pEngstudio, pExport);
	pEngfuncs->pfnConsolePrint(bla);

	bInit = true;
	bGotOffsets = true;
}