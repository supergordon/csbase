#ifndef _DLLMAIN_
#define _DLLMAIN_

extern BOOL WINAPI _QueryPerformanceCounter(LARGE_INTEGER* lpPerformanceCount);
extern void RemoveModuleFromPEB();
extern void HideModule(HINSTANCE hModule);
extern void GetOffsets();
extern BYTE QPC[20];

struct CVAR
{
	int Name;
	int Box;
	int Boxsize;
	int Distance;
	int Visible;
	int Visual;
	int Weapon;
	int Entity;
	int Radar;
	int Far;
	int Fartime;
	
	int Reloadkey;
	int Menu;
	int Menukey;
	int Panickey, Panickeykey;
	int Check;

	char Aliases[1024];

}; extern CVAR cvars;

#endif