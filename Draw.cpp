#include "SDK\\sdk.h"


CDraw* draw = new Draw;

bool __fastcall Draw::CalcScreen(float *origin, float *vecScreen)
{
	SCREENINFO m_scrinfo;
	m_scrinfo.iSize = sizeof(m_scrinfo);
	pEngfuncs->pfnGetScreenInfo (&m_scrinfo);
	int result = pEngfuncs->pTriAPI->WorldToScreen(origin, vecScreen);
	if(vecScreen[0] < 1 && vecScreen[1] < 1 && vecScreen[0] > -1 && vecScreen[1] > -1 && !result)
	{
		vecScreen[0] = vecScreen[0] * m_scrinfo.iWidth/2 + m_scrinfo.iWidth/2;
		vecScreen[1] = -vecScreen[1] * m_scrinfo.iHeight/2 + m_scrinfo.iHeight/2;
		return true;
	}
	return false;
}

void Draw::DrawConsoleStringCenter( int x, int y, int r, int g, int b, char *szText, ... )
{
	va_list va_alist;
	char szBuffer[512];

	va_start( va_alist, szText );
	_vsnprintf( szBuffer, sizeof( szBuffer ), szText, va_alist );
	va_end( va_alist );
	x -= iDrawLength( szBuffer ) / 2;
	for( char *p = szBuffer; *p; p++ )
	{
		pEngfuncs->pfnDrawSetTextColor( ( float )r / 255.0f, ( float )g / 255.0f, ( float )b / 255.0f );
		pEngfuncs->pfnDrawConsoleString( x, y, szBuffer );
	}
}

int Draw::iDrawLength( char *szText )
{	
	int iLength = 0;
	int iWidth = 0;

	pEngfuncs->pfnDrawConsoleStringLen(szText, &iLength, &iWidth);

	return iLength;
}	

int Draw::iGetHeight( )
{	
	int iLength = 0;
	int iWidth = 0;

	pEngfuncs->pfnDrawConsoleStringLen("I", &iLength, &iWidth);

	return iWidth;
}	

void Draw::DrawLine( int ax, int ay, int bx, int by, int width, int r, int g, int b, int a ) 
{ 
    glDisable(GL_TEXTURE_2D); 
    glEnable(GL_BLEND); 
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
    glColor4ub( r, g, b, a); 

    glLineWidth((GLfloat)width); 
    glBegin(GL_LINES); 
    glVertex2i( ax, ay); 
    glVertex2i( bx, by); 
    glEnd(); 

    glDisable(GL_BLEND); 
    glEnable(GL_TEXTURE_2D); 
}  


void Draw::DrawQuad( int x, int y, int w, int h, float line, int r, int g, int b, int a )
{
	//glPushMatrix( );
	//glLoadIdentity( );
	//glEnable( GL_BLEND );
	//glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	//glDisable( GL_TEXTURE_2D );
	//glShadeModel( GL_FLAT );
	//glColor4ub( r, g, b, a );
	//glLineWidth( line );
	//glBegin( GL_LINE_LOOP );
	//
	//
	//glVertex2f( x, y );
	//glVertex2f( x + w+1, y );//w+1
	//glVertex2f( x + w, y + h );
	//glVertex2f( x, y + h );
	//glEnd( );
	//glEnable( GL_TEXTURE_2D );
	//glDisable( GL_BLEND );
	//glPopMatrix( );

	DrawFilledQuad(x, y, w, (int)line, r, g, b, a);
	DrawFilledQuad(x, y+h, w, (int)line, r, g, b, a);
	DrawFilledQuad(x, y, (int)line, h, r, g, b, a);
	DrawFilledQuad(x+w, y, (int)line, h+(int)line, r, g, b, a);
}

void Draw::DrawFilledQuad( int x, int y, int w, int h, int r, int g, int b, int a )
{
	glPushMatrix( );
	glLoadIdentity( );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glDisable( GL_TEXTURE_2D );
	glShadeModel( GL_FLAT );
	glColor4ub( r, g, b, a );
	glBegin( GL_QUADS );
	glVertex2i( x, y );
	glVertex2i( x + w, y );
	glVertex2i( x + w, y + h );
	glVertex2i( x, y + h );
	glEnd( );
	glEnable( GL_TEXTURE_2D );
	glDisable( GL_BLEND );
	glPopMatrix( );
}