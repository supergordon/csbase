#ifndef _DRAW_
#define _DRAW_

class CDraw
{
	public:
		virtual bool __fastcall CalcScreen(float *origin, float *vecScreen){return 0;}
		virtual int iDrawLength( char *szText ){return 0;};
		virtual int iGetHeight(){return 0;};
		virtual void DrawLine( int ax, int ay, int bx, int by, int width, int r, int g, int b, int a ){};
		virtual void DrawQuad( int x, int y, int w, int h, float line, int r, int g, int b, int a ){};
		virtual void DrawFilledQuad( int x, int y, int w, int h, int r, int g, int b, int a ){};
		virtual void DrawConsoleStringCenter( int x, int y, int r, int g, int b, char *szText, ... ){};

};

class Draw: public CDraw
{
	public:
		bool __fastcall CalcScreen(float *origin, float *vecScreen);
		int iDrawLength( char *szText );
		int iGetHeight();
		void DrawLine( int ax, int ay, int bx, int by, int width, int r, int g, int b, int a );
		void DrawBox( int x, int y, int z, int radius, float line, int r, int g, int b, int a );
		void DrawQuad( int x, int y, int w, int h, float line, int r, int g, int b, int a );
		void DrawFilledQuad( int x, int y, int w, int h, int r, int g, int b, int a );
		void DrawConsoleStringCenter( int x, int y, int r, int g, int b, char *szText, ... );
};

extern CDraw* draw;

#endif