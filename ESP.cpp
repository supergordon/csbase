#include "SDK\\sdk.h"


ESP* esp = new ESP;

#define COPY(r, g, b, a) { gPlayers[id]->rgba[0] = r; gPlayers[id]->rgba[1] = g; gPlayers[id]->rgba[2] = b; gPlayers[id]->rgba[3] = a;}
#define COL(index) gPlayers[id]->rgba[index]


#define M_RADPI 57.295779513082f
void CalcAngle(float *vSource, float* vDest, float* vAngles )
{
	double delta[3] = { (vSource[0]-vDest[0]), (vSource[1]-vDest[1]), (vSource[2]-vDest[2]) };
	double hyp = sqrt( delta[0]*delta[0] + delta[1]*delta[1] );

	vAngles[0] = (float) (atan(delta[2]/hyp) * M_RADPI);
	vAngles[1] = (float) (atan(delta[1]/delta[0]) * M_RADPI);
	vAngles[2] = 0.0f;

	if( delta[0] >= 0.0f ) vAngles[1] += 180.0f;
}

#define M_PI 3.14159265358979f
void VectorAngles( const float *forward, float *angles )
{
	float	tmp, yaw, pitch;
	
	if (forward[1] == 0 && forward[0] == 0)
	{
		yaw = 0;
		if (forward[2] > 0)
			pitch = 90.0;
		else
			pitch = 270.0;
	}
	else
	{
		yaw = (float)(atan2(forward[1], forward[0]) * 180.0 / M_PI);
		
		if (yaw < 0) yaw += 360.0;

		tmp = (float)sqrt (forward[0]*forward[0] + forward[1]*forward[1]);
		
		pitch = (float)(atan2(forward[2], tmp) * 180 / M_PI);
	}
	
	angles[0] = pitch;
	angles[1] = yaw;
	angles[2] = 0;
}

float Degree(float radian) 
{ 
    return radian * (180/M_PI); 
} 
//===================================================================================
void MakeDelta(float *src, float *dst, float *vec) 
{ 
    vec[0] = src[0] - dst[0]; 
    vec[1] = src[1] - dst[1]; 
    vec[2] = src[2] - dst[2]; 
} 
//===================================================================================
void MakeAngle(float *vec, float *angle) 
{ 
    float hyp = sqrt(vec[0]*vec[0] + vec[1]*vec[1]); 
    angle[0] = Degree(atan(vec[2]/hyp)); 
    angle[2] = 0.0f; 
    if(vec[0] >= 0) 
    { 
        if(vec[1] >= 0)        {    angle[1] = Degree(atan(fabs(vec[1]/vec[0]))) + 180;    } 
        else                {    angle[1] = Degree(atan(fabs(vec[0]/vec[1]))) + 90;    } 
    } 
    else 
    { 
        if(vec[1] >= 0)        {    angle[1] = Degree(atan(fabs(vec[0]/vec[1]))) + 270;    } 
        else                {    angle[1] = Degree(atan(fabs(vec[1]/vec[0])));        } 
    } 
} 
extern float mainViewOrigin[3];
float fLocal[3]={0}, fEntity[3] = {0};
pmtrace_t vis = {0};
hud_player_info_t pInfo = {0};
void ESP::Main(int id)
{	
	if(id > 32)
		return;
	
	pEngfuncs->pfnGetPlayerInfo(id, &pInfo);
	cl_entity_s* pLocal = pEngfuncs->GetLocalPlayer();
	
	if(id == pLocal->index)
		return;
	
	cl_entity_s* pEntity = pEngfuncs->GetEntityByIndex(id);
	
	
	pEngfuncs->pEventAPI->EV_SetTraceHull( 2 ); 

	VectorCopy(pLocal->origin,fLocal);
	VectorCopy(pEntity->origin,fEntity);
	fEntity[2] += 20;
	fLocal[2] += 20;

	pEngfuncs->pEventAPI->EV_PlayerTrace( fLocal, gPlayers[id]->vHitbox, PM_TRACELINE_PHYSENTSONLY, -1, &vis );

	Height = 0;
	int AddHeight = 13;


	if(isValidEnt(pEntity))
	{
		
		if(gPlayers[id]->team == 1)
			COPY(255, 2, 0, 255)
		else if(gPlayers[id]->team == 2)
			COPY(0, 100, 255, 255)
		else
			COPY(255,2,0,255);

		if(gPlayers[id]->vHitbox[0] != 0)
		{
			if(!draw->CalcScreen(gPlayers[id]->vHitbox, Pos)/* && pInfo.name*/)
				return;

			gPlayers[id]->vHitbox[0] = 0;

			if(Pos[0] < 10 && Pos[1] < 10)
				return;

			
			if(vis.fraction >= 1.0f && gPlayers[pLocal->index]->team != gPlayers[id]->team) {
				COPY(0, 255, 0, 255)
			
				//this->CalcAngle(pLocal->EyePosition(), this->vPlayer[i], qAimAngles);
				/*
				VectorAngles(m_vTraceVector, m_vAimAngles);

				MakeDelta(mainViewOrigin, out, vector);
				MakeAngle(vector, angle); 

				*/
				float aimangles[3] = { 0 };
				float lol[3] = {0};
				MakeDelta(mainViewOrigin, gPlayers[id]->vHitbox, aimangles);
				MakeAngle(aimangles, lol);
				pEngfuncs->SetViewAngles(lol);
			}
					
			draw->DrawFilledQuad(Pos[0], Pos[1], 3,3, COL(0), COL(1), COL(2), 255);
		}
	}
	else
	{
		gPlayers[pEntity->index]->bGotHead = false; 
		gPlayers[id]->bGotHead = false; 
	}
}