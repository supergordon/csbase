#ifndef _ESP_
#define _ESP_

#include "SDK\\SDK.h"

class ESP
{
	public:
		void Main(int id);

		float Pos[3];
		float Far[3];
		int rgba[3];
		int Height;
		int FarHeight;
};

extern ESP* esp;

#endif