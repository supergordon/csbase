#include <windows.h>
#include <iostream>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>

#include "OGLStuff.h"
#include "Client.h"

OGLStuff* ogls = new OGLStuff;

void OGLStuff::XQZ(GLenum mode, int* toggle)
{
	if(*toggle)
	{
		cl_entity_s* ent = pEngstudio->GetCurrentEntity();
			
		if( ent && ent->player )
		{
			glDepthRange( 0, 0.5 );
		}
		else
		{
			glDepthRange( 0.5, 1 );
		}
	}
}

void OGLStuff::NoFlash(int* toggle)
{
	if(*toggle)
	{
		static screenfade_t sf = { 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 0, 0 };
		pEngfuncs->pfnSetScreenFade( &sf );
	}
}

void OGLStuff::Fullbright(GLenum mode, int* toggle)
{
	if(*toggle)
	{
		if(pEngstudio->GetCurrentEntity())
		{
			cl_entity_s* ent = pEngstudio->GetCurrentEntity();
			if(!ent->player)
				ent->curstate.rendermode=kRenderGlow;
		}
	}
}

void OGLStuff::Whitewall(GLenum mode, int* toggle)
{
	if(*toggle)
	{
		if(pEngstudio->GetCurrentEntity())
		{
			cl_entity_s* ent = pEngstudio->GetCurrentEntity();
			if(mode == GL_POLYGON && !ent->player) 
				glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL); 
		}
	}
}

void OGLStuff::Lambert(GLenum mode, int* toggle)
{
	if(*toggle)
	{
		cl_entity_s* ent = pEngstudio->GetCurrentEntity();
			
		if( ent && ent->player )
		{
			if(mode == GL_TRIANGLE_STRIP || mode == GL_TRIANGLE_FAN)
				glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
		}
	}
}

void OGLStuff::Niggamodels(GLenum mode, int* toggle)
{
	if(*toggle)
	{
		cl_entity_s* ent = pEngstudio->GetCurrentEntity();	
		if( ent && ent->player )
		{
			if(mode == GL_TRIANGLE_STRIP)
				glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
		}
	}
}

void OGLStuff::HUD(GLenum dfactor, int r, int g, int b, int* toggle)
{
	if(*toggle)
	{
		if(dfactor == GL_ONE)
			glColor3ub(r,g,b);
	}
}

void OGLStuff::Nosmoke(GLenum mode, int* toggle)
{
	if(*toggle)
	{
		ogls->bSmoke = 1;
		if (mode == GL_QUADS) 
		{ 
			bQuads = true; 
			glGetFloatv(GL_CURRENT_COLOR, color); 
				
			if((color[0]==color[1]) && (color[0] == color[2]) && (color[0] != 0.0) && (color[0] != 1.0)) 
			bSmoke = true; else bSmoke = false; 
		}
		else bQuads = false;
	}
	else
		ogls->bSmoke = 0;
}

void OGLStuff::ModelWireframe(GLenum mode, int* toggle)
{
	if(*toggle)
	{
		cl_entity_s* ent = pEngstudio->GetCurrentEntity();	
		if( ent && ent->player )
		{
			if(mode != GL_POLYGON && (mode==GL_TRIANGLES||mode==GL_TRIANGLE_STRIP||mode==GL_TRIANGLE_FAN))
			{
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				glLineWidth(2);
			} 
			else 
			{ 
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			}
		}
	}
}