#ifndef _OGLSTUFF_
#define _OGLSTUFF_

#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>

class OGLStuff
{
	public:
		void XQZ(GLenum mode, int* toggle);
		void NoFlash(int* toggle);
		void Fullbright(GLenum mode,int* toggle);
		void Whitewall(GLenum mode, int* toggle);
		void Lambert(GLenum mode, int* toggle);
		void Niggamodels(GLenum mode, int* toggle);
		void HUD(GLenum dfactor, int r, int g, int b, int* toggle);
		void Nosmoke(GLenum mode, int* toggle);
		void ModelWireframe(GLenum mode, int* toggle);

		GLfloat color[4];
		bool bFlash;
		bool bScope;
		bool bSky;
		bool bSmoke;
		bool bClearbit;
		bool bQuads;
		bool bIniT;
		bool bPolygon;
		int polyCoords; 
		float polyArray[32][3];

};

extern OGLStuff* ogls;

#endif