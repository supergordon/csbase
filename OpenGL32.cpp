#include <windows.h>
#include <iostream>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>

#include "OpenGL32.h"
#include "OGLStuff.h"
#include "Player.h"
#include "client.h"
#include "Other\\detours.h"

COpenGL32 ogl;

typedef void (WINAPI *glBegin_t)(GLenum); glBegin_t Org_glBegin = NULL;
typedef void (WINAPI *glVertex3f_t)(GLfloat, GLfloat, GLfloat); glVertex3f_t Org_glVertex3f = NULL;
typedef void (WINAPI *glClear_t)(GLbitfield); glClear_t Org_glClear = NULL;;
typedef void (WINAPI *glVertex3fv_t)(const GLfloat*); glVertex3fv_t Org_glVertex3fv = NULL;
typedef bool (WINAPI *wglSwapBuffers_t)(HDC); wglSwapBuffers_t Org_wglSwapBuffers = NULL;
typedef void (WINAPI *glViewport_t) (GLint x,  GLint y,  GLsizei width,  GLsizei height); glViewport_t Org_glViewport = NULL;
typedef void (WINAPI *glPopMatrix_t) (void); glPopMatrix_t Org_glPopMatrix = NULL;
typedef void (WINAPI *glEnd_t) (void); glEnd_t Org_glEnd = NULL;
typedef void (WINAPI *glBlendFunc_t) (GLenum sfactor, GLenum dfactor); glBlendFunc_t Org_glBlendFunc = NULL;

#define ADD(name) Org_##name = (name##_t)DetourFunction((LPBYTE)name,(LPBYTE)_##name)
#define DEL(name) DetourRemove((LPBYTE)Org_##name,(LPBYTE)_##name);


void thread()
{
	char lola[255] = "";
	sprintf(lola,"pEngfuncs: 0x%X\npEngstudio: 0x%X\npExport: 0x%X\nSound: 0x%X\n",pEngfuncs,pEngstudio,pExport,SOUND);
	pEngfuncs->pfnConsolePrint(lola);
}
void WINAPI _glBegin(GLenum mode)
{
	static bool bInit = false;
	if(!bInit)
	{
	//	ADD(glViewport);
		pEngfuncs->pfnClientCmd("toggleconsole");
		CreateThread(0,0,(LPTHREAD_START_ROUTINE)thread,0,0,0);
		bInit = true;
	}
	
	return (*Org_glBegin)(mode);
}

void WINAPI _glBlendFunc (GLenum sfactor, GLenum dfactor)
{
	return (*Org_glBlendFunc)(sfactor, dfactor);
}

void WINAPI _glEnd(void)
{

	return (*Org_glEnd)();
}

void WINAPI _glVertex3f(GLfloat x, GLfloat y, GLfloat z)
{

	return (*Org_glVertex3f)(x,y,z);
}

void WINAPI _glClear(GLbitfield mask)
{

	return (*Org_glClear)(mask);
}

void WINAPI _glVertex3fv(const GLfloat *v) 
{

	return (*Org_glVertex3fv)(v);
}


void WINAPI _glViewport (GLint x,  GLint y,  GLsizei width,  GLsizei height)
{

	return (*Org_glViewport)(x,y,width,height);
}

void WINAPI _glPopMatrix(void)
{

	return (*Org_glPopMatrix)();
}

bool WINAPI _wglSwapBuffers(HDC hDC)
{
	
	return (*Org_wglSwapBuffers)(hDC);
}

void COpenGL32::InitOGL()
{
	
//	ADD(glViewport);
	ADD(glBegin);
	//ADD(glEnd);
	//ADD(glVertex3f);
	//ADD(glVertex3fv);
	//ADD(glClear);
	//ADD(glPopMatrix);
	//ADD(glBlendFunc);
	//ADD(wglSwapBuffers);
	
	return;
}

void COpenGL32::UnInitOGL()
{
	
	DEL(glViewport);
	DEL(glBegin);
	//DEL(glEnd);
	//DEL(glVertex3f);
	//DEL(glVertex3fv);
	//DEL(glClear);
	//DEL(glPopMatrix);
	//DEL(glBlendFunc);
	//DEL(wglSwapBuffers);

	return;
}