#ifndef _OPENGL32_
#define _OPENGL32_

#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>

extern void WINAPI _glBegin(GLenum mode);
extern void WINAPI _glVertex3f(GLfloat x, GLfloat y, GLfloat z);
extern void WINAPI _glClear(GLbitfield mask);
extern void WINAPI _glVertex3fv(const GLfloat *v);
extern void WINAPI _glViewport(GLint x,  GLint y,  GLsizei width,  GLsizei height);
extern void WINAPI _glPopMatrix(void);
extern bool WINAPI _wglSwapBuffers(HDC hDC);


class COpenGL32
{
	public:
		void InitOGL();
		void UnInitOGL();
};

extern COpenGL32 ogl;

#endif