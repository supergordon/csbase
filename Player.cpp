#include <iostream>
#include <windows.h>
#include <vector>
#include <fstream>
#include <string>

#include "DLLMain.h"
#include "Client.h"
#include "Player.h"
#include "Other\\detours.h"
#include "SDK\\sdk.h"

using namespace std;


std::vector<CPlayer*> gPlayers;


void InitPlayers()
{
	for (int i = 0; i < 33; i++)
	{
		CPlayer *gPlayer = new CPlayer;
		gPlayer->id = i;
		gPlayers.push_back(gPlayer);
	}
}

void DeletePlayers()
{
	for (int i = 0; gPlayers[i]->id; i++)
		delete gPlayers[i];
}

bool isValidEnt(cl_entity_s *ent)
{
	if(ent && (ent != pEngfuncs->GetLocalPlayer()) && !(ent->curstate.effects & EF_NODRAW) && ent->player && !ent->curstate.spectator && ent->curstate.solid && !(ent->curstate.messagenum < pEngfuncs->GetLocalPlayer()->curstate.messagenum)) 
	{
		return true;
	}
	else 
	{
		return false;
	}
}

float fGetDistance( float *src, float *dst )
{
	float float1[3];
	float1[0] = dst[0] - src[0];
	float1[1] = dst[1] - src[1];
	float1[2] = dst[2] - src[2];
	return sqrt( float1[0] * float1[0] + float1[1] * float1[1] + float1[2] * float1[2] );	
} 