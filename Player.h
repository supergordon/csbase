#ifndef _PLAYER_
#define _PLAYER_

#include <vector>
#include "SDK\\sdk.h"


class CPlayer
{
	public:
		
		bool bAlive;
		bool bGotSound;
		bool bFilter;
		int id;
		int ping;
		int team;
		char name[255];
		char weapon[255];
		float distance;
		float Position[3];
		float fSoundOrigin[3];
		float fFar[3];
		int sequence;
		int gaitseq;
		int rgba[4];
		bool bGotHead;
		vec3_t vHitbox;
		DWORD dwTime;
		cl_entity_s* ent;
};


extern std::vector<CPlayer*> gPlayers;

extern void InitPlayers();
extern void DeletePlayers();
extern bool isValidEnt(cl_entity_s *ent);
extern float fGetDistance( float *src, float *dst );
extern int GetTeam(char *model);
extern int Myteam(void);

#endif