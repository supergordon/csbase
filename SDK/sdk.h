#ifndef _SDK_
#define _SDK_

#pragma warning(disable: 4311 4312 4244 4305 4102 4267)

#include "Engine\wrect.h"
#include "Engine\cl_dll.h"
#include "Engine\cdll_int.h"
#include "Engine\const.h"
#include "Engine\progdefs.h"
#include "Engine\eiface.h"
#include "Engine\edict.h"
#include "Engine\studio_event.h"
#include "Engine\entity_types.h"
#include "Engine\r_efx.h"
#include "Engine\net_api.h"
#include "Engine\demo_api.h"
#include "Engine\triangleapi.h"
#include "Engine\event_api.h"
#include "Engine\pm_defs.h"
#include "Engine\ref_params.h"
#include "Engine\r_studioint.h"
#include "Engine\com_model.h"
#include "Common\screenfade.h"
#include "Engine\studio.h"
#include "Misc\defs.h"
#include "Common\cl_entity.h"
#include "Misc\parsemsg.h"

typedef struct ExportTable_s
{
	int (*Initialize)(cl_enginefunc_t *, int);
	int (*HUD_Init)(void);
	int (*HUD_VidInit)(void);
	int (*HUD_Redraw)(float, int);
	int (*HUD_UpdateClientData)(client_data_t*, float);
	int (*HUD_Reset)(void);
	void (*HUD_PlayerMove)(struct playermove_s*, int);
	void (*HUD_PlayerMoveInit)(struct playermove_s*);
	char (*HUD_PlayerMoveTexture)(char *);
	void (*IN_ActivateMouse)(void);
	void (*IN_DeactivateMouse)(void);
	void (*IN_MouseEvent)(int mstate);
	void (*IN_ClearStates)(void);
	void (*IN_Accumulate)(void);
	void (*CL_CreateMove)(float, struct usercmd_s*, int);
	int (*CL_IsThirdPerson)(void);
	void (*CL_CameraOffset)(float *);
	struct kbutton_s *(*KB_Find)(const char*);
	void (*CAM_Think)(void);
	void (*V_CalcRefdef)(struct ref_params_s *pparams);
	int (*HUD_AddEntity)(int, struct cl_entity_s *, const char *);
	void (*HUD_CreateEntities)(void);
	void (*HUD_DrawNormalTriangles)(void);
	void (*HUD_DrawTransparentTriangles)(void);
	void (*HUD_StudioEvent)(const struct mstudioevent_s *, const struct cl_entity_s *);
	void (*HUD_PostRunCmd)(struct local_state_s*, struct local_state_s*, struct usercmd_s*, int, double, unsigned int);
	void (*HUD_Shutdown)(void);
	void (*HUD_TxferLocalOverrides)(struct entity_state_s *, const struct clientdata_s *);
	void (*HUD_ProcessPlayerState)(struct entity_state_s *, const struct entity_state_s *);
	void (*HUD_TxferPredictionData)(struct entity_state_s *, const struct entity_state_s *, struct clientdata_s *, const struct clientdata_s *, struct weapon_data_s *, const struct weapon_data_s *);
	void (*Demo_ReadBuffer)(int, unsigned char *);
	int (*HUD_ConnectionlessPacket)(struct netadr_s*, const char*, char*, int*);
	int (*HUD_GetHullBounds)(int, float*, float*);
	void (*HUD_Frame)(double time);
	int (*HUD_Key_Event)(int, int, const char *);
	void (*HUD_TempEntUpdate)(double, double, double, struct tempent_s **, struct tempent_s **, int(*Callback_AddVisibleEntity)(struct cl_entity_s*), void(*Callback_TempEntPlaySound)(struct tempent_s *pTemp, float damp ));
	struct cl_entity_s *(*HUD_GetUserEntity)(int index);
	int (*HUD_VoiceStatus)(int entindex, qboolean bTalking);
	int (*HUD_DirectorEvent) (unsigned char, unsigned int, unsigned int, unsigned int);
	int (*HUD_GetStudioModelInterface)(int, struct r_studio_interface_s**, struct engine_studio_api_s*); 
	unsigned long (*ClientFactory )(void);
} ExportTable_t;

#define _WIN32_WINNT 0x0500

#include <iostream>
#include <windows.h>
#include <fstream>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>

#pragma comment(lib, "opengl32.lib")

using namespace std;

#include "Client.h"
#include "DLLMain.h"
#include "Player.h"
#include "Draw.h"
#include "ESP.h"
#include "Usermsg.h"
#include "Other\\detours.h"

#endif