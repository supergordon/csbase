#include "SDK\\sdk.h"


typedef struct usermsg_s 
{ 
int number; 
int size; 
char name[16]; 
struct usermsg_s* next; 
pfnUserMsgHook pfn; 
} usermsg_t;


usermsg_t** gUserMsgList = NULL;
pfnUserMsgHook pResetHUD;
pfnUserMsgHook pDeathMsg;
pfnUserMsgHook pTeamInfo;

static int TeamInfo (const char *pszName, int iSize, void *pbuf)
{
    BEGIN_READ(pbuf,iSize);
	int id = READ_BYTE();
	char *team = READ_STRING();

	if(strstr(team, "TERRORIST"))
		gPlayers[id]->team = 1;
	else if(strstr(team, "CT"))
		gPlayers[id]->team = 2;

	return pTeamInfo(pszName, iSize, pbuf);
}

#define COPY(x, y, z) { gPlayers[victim]->fSoundOrigin[0] = x; gPlayers[victim]->fSoundOrigin[1] = y; gPlayers[victim]->fSoundOrigin[2] = z;}

static int DeathMsg (const char *pszName, int iSize, void *pbuf)
{
	BEGIN_READ( pbuf, iSize );
	int killer = READ_BYTE();
	int victim = READ_BYTE();
	int headshot = READ_BYTE();

	COPY(0, 0, 0);
	gPlayers[victim]->bAlive = false;

	return pDeathMsg(pszName, iSize, pbuf);
}

static int ResetHUD(const char *pszName, int iSize, void *pbuf)
{
	AtRoundStart();
	return pResetHUD(pszName, iSize, pbuf);
}


pfnUserMsgHook InterceptUserMsg( const char* pszName, pfnUserMsgHook pMsgHook )
{
    if ( gUserMsgList == NULL )
		return NULL;

	usermsg_t* msg = *gUserMsgList;
	pfnUserMsgHook pReturn = NULL;

	while ( msg )
	{
		if( strcmp( msg->name, pszName ) == 0 )
		{
			pReturn = msg->pfn;
			msg->pfn  = pMsgHook;
			break;
		}
		msg = msg->next;
	}

	return pReturn;
}

void SetupUserMessageHooks()
{
	static bool bApplied = false;

	if (bApplied == false)
	{
		bApplied = true;
			
		DWORD	dwAddress = NULL;

		dwAddress = (DWORD)pEngfuncs->pfnHookUserMsg + 0x1B;
		dwAddress += *(int*)( dwAddress ) + 4;
		dwAddress += 0xD;
		gUserMsgList	= (usermsg_t**)			 (*(DWORD*)( dwAddress ));
		
		pTeamInfo		= InterceptUserMsg( "TeamInfo", TeamInfo);
		pResetHUD		= InterceptUserMsg( "ResetHUD", ResetHUD);
		pDeathMsg		= InterceptUserMsg( "DeathMsg", DeathMsg);
	}
}