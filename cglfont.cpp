#include "SDK\\sdk.h"


#pragma comment(lib, "opengl32.lib")


CGLFont *font = new CGLFont;

char fontbuf[1024];

int font_color_table[10][3] = 
{
//    r,   g,   b
{	255, 255, 255	},		//0
{	255,   0,   0	},		//1
//{	  0, 152,   0	},		//2
{	  0, 192,   0	},		//2
{	255, 248,   0	},		//3
//{	  0,   0, 152	},		//4
{	  0,   0, 255	},		//4
{	  0, 204, 255	},		//5
{	255, 180,   0	},		//6
{	120,   0,   0	},		//7
{	  0,  71, 145	},		//8
{	  0,   0,   0	}		//9
};


CGLFont::CGLFont()
{

}

CGLFont::CGLFont(int h, int weight, char *name, bool _italic, bool _underline, bool _bold)
{
	Build(h,weight,name,_italic,_underline,_bold);
}


CGLFont::~CGLFont()
{
	Kill();
}


void CGLFont::Build(int h, int weight, char *name, bool _italic, bool _underline, bool _bold)
{
	Kill();

	HFONT hFont;
	HFONT hiFont;
	HFONT huFont;
	HFONT hbFont;

	hDC = wglGetCurrentDC();

	HFONT oldfont;
	SIZE s;
	char tmpc[2] = {'\0','\0'};
	int i;

	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	base = glGenLists(NUM_GLYPHS);
	hFont = CreateFont(h, 0, 0, 0, weight, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY /*| ANTIALIASED_QUALITY*/, FF_DONTCARE | DEFAULT_PITCH, name);
	if(!hFont)
		return;
	oldfont = (HFONT)SelectObject(hDC, hFont);
	wglUseFontBitmaps(hDC, 0, NUM_GLYPHS, base);

	for(i=0; i < NUM_GLYPHS; i++)
	{
		tmpc[0] = (char)i;
		GetTextExtentPoint32(hDC,tmpc,1,&s);
		sizes[i] = (char)s.cx;
	}

	if(_italic)
	{
		italic = true;
		ibase = glGenLists(NUM_GLYPHS);
		hiFont = CreateFont(h, 0, 0, 0, weight, TRUE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY /*| ANTIALIASED_QUALITY*/, FF_DONTCARE | DEFAULT_PITCH, name);
		SelectObject(hDC, hiFont);
		wglUseFontBitmaps(hDC, 0, NUM_GLYPHS, ibase);
		for(i=0; i < NUM_GLYPHS; i++)
		{
			tmpc[0] = (char)i;
			GetTextExtentPoint32(hDC,tmpc,1,&s);
			isizes[i] = (char)s.cx;
		}
	}

	if(_underline)
	{
		underline = true;
		ubase = glGenLists(NUM_GLYPHS);
		huFont = CreateFont(h, 0, 0, 0, weight, FALSE, TRUE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY /*| ANTIALIASED_QUALITY*/, FF_DONTCARE | DEFAULT_PITCH, name);
		SelectObject(hDC, huFont);
		wglUseFontBitmaps(hDC, 0, NUM_GLYPHS, ubase);
		for(i=0; i < NUM_GLYPHS; i++)
		{
			tmpc[0] = (char)i;
			GetTextExtentPoint32(hDC,tmpc,1,&s);
			usizes[i] = (char)s.cx;
		}
	}

	if(_bold)
	{
		bold = true;
		bbase = glGenLists(NUM_GLYPHS);
		hbFont = CreateFont(h, 0, 0, 0, weight + 300, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY /*| ANTIALIASED_QUALITY*/, FF_DONTCARE | DEFAULT_PITCH, name);
		SelectObject(hDC, hbFont);
		wglUseFontBitmaps(hDC, 0, NUM_GLYPHS, bbase);
		for(i=0; i < NUM_GLYPHS; i++)
		{
			tmpc[0] = (char)i;
			GetTextExtentPoint32(hDC,tmpc,1,&s);
			bsizes[i] = (char)s.cx;
		}
	}

	SelectObject(hDC, oldfont);

	if(hFont)
		DeleteObject(hFont);
	
	if(hiFont)
		DeleteObject(hiFont);
	
	if(huFont)
		DeleteObject(huFont);
	
	if(hbFont)
		DeleteObject(hbFont);
}

void CGLFont::Kill()
{
	italic = false;
	underline = false;
	bold = false;

	if(base)
		glDeleteLists(base, NUM_GLYPHS);
	if(ibase)
		glDeleteLists(ibase, NUM_GLYPHS);
	if(ubase)
		glDeleteLists(ubase, NUM_GLYPHS);
	if(bbase)
		glDeleteLists(bbase, NUM_GLYPHS);
}


int CGLFont::RawDrawLen(char *c, char *_sizes)
{
	char *tmp = c;
	int ret = 0;
	while(*tmp)
	{
		if(*tmp < NUM_GLYPHS)
			ret += _sizes[*tmp];
		tmp++;
	}
	return ret;
}

int CGLFont::DrawLen(char c)
{
	char cc[2];
	cc[0] = c;
	cc[1] = '\0';
	return RawDrawLen(cc,sizes);
}

int CGLFont::DrawLen(char *c)
{
	return RawDrawLen(c,sizes);
}

int CGLFont::DrawLenX(char *c)
{
	char *cursizes = sizes;
	int curcol = 0;

	char *tmp = c;
	char pbuf[1024];
	char *pp;

	int cw = 0;

	for(;;)
	{
		pp = pbuf;
		for(;;){
			if(!*tmp || *tmp == '^')
				break;
			*pp = *tmp;
			pp++;
			tmp++;
		}
		*pp = '\0';

		if(pbuf[0])
		{
			cw += RawDrawLen(pbuf,cursizes);
		}
			
		if(*tmp == '^')
		{
			tmp++;
			if(*tmp)
			{
				char tmpc = *tmp;
				if(tmpc == 'a' || tmpc == 'n' || tmpc == 'x')
				{
					cursizes = sizes;
				}
				else if(tmpc == 'i' && italic)
				{
					cursizes = isizes;
				}
				else if(tmpc == 'u' && underline)
				{
					cursizes = usizes;
				}
				else if(tmpc == 'b' && bold)
				{
					cursizes = bsizes;
				}
				else if(tmpc >= '0' && tmpc <= '9')
				{
				}
				else
				{
					*pp = '^';
					pp++;
					goto skip_font_code;
				}
				tmp++;
			}
		}
skip_font_code:

		if(!*tmp)
			break;
	}

	return cw;
}

int CGLFont::DrawLenX(char c)
{
	char cc[2];
	cc[0] = c;
	cc[1] = '\0';
	return DrawLenX(cc);
}

void CGLFont::RawPrint(int x, int y, int r, int g, int b, int a, char *text, GLuint _base)
{
	GLfloat curcolor[4], position[4];

	y -= 4;

	glPushAttrib(GL_LIST_BIT);
	glGetFloatv(GL_CURRENT_COLOR, curcolor);
	glGetFloatv(GL_CURRENT_RASTER_POSITION, position);
	glDisable(GL_TEXTURE_2D);

	glColor4ub(r,g,b,a);
	glRasterPos2i(x,y);

	glListBase(_base);
	glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);

	glPopAttrib();
	glColor4fv(curcolor);
	glRasterPos2f(position[0],position[1]);
	glEnable(GL_TEXTURE_2D);

}

void CGLFont::Print(int x, int y, int r, int g, int b, int a, bool shadow, char *text)
{
	if(shadow)
	{
		RawPrint(x+1,y+1,0,0,0,245,text,base);
	}
	RawPrint(x,y,r,g,b,a,text,base);
}

void CGLFont::Print(int x, int y, int r, int g, int b, int a, bool shadow, const char *fmt, ...)
{
	va_list ap;										
	
	if(fmt == NULL)
		return;

	va_start(ap, fmt);
	vsprintf(fontbuf, fmt, ap);
	va_end(ap);

	if(shadow)
	{
		Print(x+1,y+1,0,0,0,245,shadow,fontbuf);
	}
	Print(x,y,r,g,b,a,shadow,fontbuf);
}

void CGLFont::Print(int x, int y, int a, char *text)
{
	GLuint curbase = base;
	char *cursizes = sizes;
	int curcol = 0;

	char *tmp = text;
	char pbuf[1024];
	char *pp = pbuf;

	int cw = 0;

	for(;;)
	{
		for(;;)
		{
			if(!*tmp || *tmp == '^')
				break;
			*pp = *tmp;
			pp++;
			tmp++;
		}
		*pp = '\0';
		pp = pbuf;

		if(pbuf[0])
		{
			RawPrint(
				x + cw,
				y,
				font_color_table[curcol][0],
				font_color_table[curcol][1],
				font_color_table[curcol][2],
				a,
				pbuf,
				curbase);
			cw += RawDrawLen(pbuf,cursizes);
			//cw++;
		}

		if(*tmp == '^')
		{
			tmp++;
			if(*tmp)
			{
				char tmpc = *tmp;
				if(tmpc == 'a' || tmpc == 'n' || tmpc == 'x')
				{
					curbase = base;
					cursizes = sizes;
				}
				else if(tmpc == 'i' && italic)
				{
					curbase = ibase;
					cursizes = isizes;
				}
				else if(tmpc == 'u' && underline)
				{
					curbase = ubase;
					cursizes = usizes;
				}
				else if(tmpc == 'b' && bold)
				{
					curbase = bbase;
					cursizes = bsizes;
				}
				else if(tmpc >= '0' && tmpc <= '9')
				{
					curcol = (int)(tmpc) - '0';
				}
				else
				{
					*pp = '^';
					pp++;
					goto skip_font_code;
				}
				tmp++;
			}
		}
skip_font_code:

		if(!*tmp)
			break;
	}

}

void CGLFont::Print(int x, int y, int a, bool shadow, const char *fmt, ...)
{
	va_list ap;										
	
	if(fmt == NULL)
		return;

	va_start(ap, fmt);
	vsprintf(fontbuf, fmt, ap);
	va_end(ap);

	if(shadow)
	{
		Print(x+1,y+1,a,fontbuf);
	}
	Print(x,y,a,fontbuf);
}

void CGLFont::CenterPrint(int x, int y, int a, bool shadow, char *text)
{
	if(shadow)
	{
		Print(x - (DrawLenX(fontbuf)/2)+1,y+1,a,text);
	}
	Print(x - (DrawLenX(text)/2),y,a,text);
}

void CGLFont::CenterPrint(int x, int y, int a, bool shadow, const char *fmt, ...)
{
	va_list ap;										
	
	if(fmt == NULL)
		return;

	va_start(ap, fmt);
	vsprintf(fontbuf, fmt, ap);
	va_end(ap);

	if(shadow)
	{
		Print(x - (DrawLenX(fontbuf)/2)+1,y+1,a,fontbuf);
	}
	Print(x - (DrawLenX(fontbuf)/2),y,a,fontbuf);
}

void CGLFont::CenterPrint(int x, int y, int r, int g, int b, int a, bool shadow, char *text)
{
	if(shadow)
	{
		RawPrint(x - (DrawLen(text)/2)+1,y+1,0,0,0,245,text,base);
	}
	RawPrint(x - (DrawLen(text)/2),y,r,g,b,a,text,base);
}

void CGLFont::CenterPrint(int x, int y, int r, int g, int b, int a, bool shadow, const char *fmt, ...)
{
	va_list ap;										
	
	if(fmt == NULL)
		return;

	va_start(ap, fmt);
	vsprintf(fontbuf, fmt, ap);
	va_end(ap);

	if(shadow)
	{
		RawPrint(x - (DrawLenX(fontbuf)/2)+1,y+1,0,0,0,245,fontbuf,base);
	}
	RawPrint(x - (DrawLenX(fontbuf)/2),y,r,g,b,a,fontbuf,base);
}