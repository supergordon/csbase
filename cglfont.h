#ifndef _CGL_
#define _CGL_

#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>

#define NUM_GLYPHS 256
class CGLFont 
{
public:
	CGLFont();
	CGLFont(int h, int weight, char *name, bool _italic, bool _underline, bool _bold);
	~CGLFont();

	virtual void Build(int h, int weight, char *name, bool _italic, bool _underline, bool _bold);
	virtual void Kill();
	
	virtual int DrawLen(char *c);
	virtual int DrawLen(char c);
	
	virtual int DrawLenX(char *c);
	virtual int DrawLenX(char c);

	virtual void Print(int x, int y, int a, char *text);
	virtual void Print(int x, int y, int a, bool shadow, const char *fmt, ...);

	virtual void Print(int x, int y, int r, int g, int b, int a, bool shadow, char *text);
	virtual void Print(int x, int y, int r, int g, int b, int a, bool shadow, const char *fmt, ...);

	virtual void CenterPrint(int x, int y, int a, bool shadow, char *text);
	virtual void CenterPrint(int x, int y, int a, bool shadow, const char *fmt, ...);

	virtual void CenterPrint(int x, int y, int r, int g, int b, int a, bool shadow, char *text);
	virtual void CenterPrint(int x, int y, int r, int g, int b, int a, bool shadow, const char *fmt, ...);
private:
	int RawDrawLen(char *c, char *_sizes);

	void RawPrint(int x, int y, int r, int g, int b, int a, char *text, GLuint _base);

	HDC hDC;
	
	GLuint base;
	char sizes[NUM_GLYPHS];

	bool italic;
	GLuint ibase;
	char isizes[NUM_GLYPHS];

	bool underline;
	GLuint ubase;
	char usizes[NUM_GLYPHS];

	bool bold;
	GLuint bbase;
	char bsizes[NUM_GLYPHS];
}; extern CGLFont *font;

#endif