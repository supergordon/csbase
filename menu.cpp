#include "SDK\\sdk.h"

cMenu gMenu;

int color[3];


int cMenu::AddEntry(int n, char title[1000], int* value, int min, int max, int step)
{
	strcpy( menuEntry[n].title,title);
	menuEntry[n].value = value;
	menuEntry[n].min = min;
	menuEntry[n].max = max;
	menuEntry[n].step = step;
	return (n+1);
}

char* DecryptString(char* string)
{
	int iSize = strlen(string);
	char *Output = new char[iSize];
	for(int i = 0; i < iSize; i++)
	{
		Output[i] = string[i]-0x10;
	}
	Output[strlen(string)] = 0;
	return Output;
}

void cMenu::Init()
{
	int i = 0;

	i = AddEntry(i, "Name", &cvars.Name, 0, 1, 1);
	i = AddEntry(i, "Box", &cvars.Box, 0, 10, 1);
	i = AddEntry(i, "Boxsize", &cvars.Boxsize, 10, 1000, 10);
	i = AddEntry(i, "Distance", &cvars.Distance, 0, 1, 1);
	i = AddEntry(i, "Visible", &cvars.Visible, 0, 1, 1);
	i = AddEntry(i, "Visual", &cvars.Visual, 0, 1, 1);
	i = AddEntry(i, "Entity", &cvars.Entity, 0, 1, 1);
	i = AddEntry(i, "Radar", &cvars.Radar, 0, 1, 1);
	i = AddEntry(i, "Far", &cvars.Far, 0, 5000, 100);
	
	menuItems = i;
}

void cMenu::Draw()
{
	if(!Active) return;

	int w = 120, h = 165; 
	int x = 100; 
	int y = 150;

	draw->DrawFilledQuad(x,y,w,h,0,0,0,255);
	draw->DrawQuad(x,y,w,h,1,255,255,255,255);
	draw->DrawLine(x,y+15,x+w,y+15,1,255,255,255,255);
	font->CenterPrint(x+(w/2),y+15,0,255,0,255,0,"Lacey 1.6");

	for(int i=0; i < menuItems; i++)
	{
		if(i == menuSelect)
		{
			color[0]=0;
			color[1]=255;
			color[2]=0;
		}
		else
		{
			color[0]=255;
			color[1]=255;
			color[2]=255;
		}

		font->Print(x+10, y+33+i*14, color[0], color[1], color[2], 255, false, menuEntry[i].title );
		
		char szMenuEntryValue[50];
		sprintf(szMenuEntryValue, "%d", menuEntry[i].value[0]);
		
		font->Print( x+8+80,y+33+i*14,255,255,255, 255,false, szMenuEntryValue );
	}
}

int cMenu::KeyEvent(int keynum)
{
	if( keynum == 128 || keynum == 240) //uparrow || mwheelup
	{
		if( menuSelect>0 ) menuSelect--;
		else menuSelect = menuItems - 1;
		return 0;
	}
	else if( keynum == 129 || keynum == 239) //downarrow || mwheeldown
	{
		if( menuSelect<menuItems-1 ) menuSelect++;
		else menuSelect = 0;
		return 0;
	}
	else if( keynum == 130 || keynum == 241 ) //leftarrow || leftbutton
	{
		if( menuEntry[menuSelect].value )
		{
			menuEntry[menuSelect].value[0] -= menuEntry[menuSelect].step;
			if( menuEntry[menuSelect].value[0] < menuEntry[menuSelect].min )
				menuEntry[menuSelect].value[0] = menuEntry[menuSelect].max;
		}
		return 0;
	}
	else if( keynum == 131 || keynum == 242 ) //rightarrow || rightbutton
	{
		if( menuEntry[menuSelect].value )
		{
			menuEntry[menuSelect].value[0] += menuEntry[menuSelect].step;
			if( menuEntry[menuSelect].value[0] > menuEntry[menuSelect].max )
				menuEntry[menuSelect].value[0] = menuEntry[menuSelect].min;
		}
		return 0;
	}
	return 1;
}