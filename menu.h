#ifndef _MENU_
#define _MENU_

#include <vector>
using namespace std;

struct menu_entrys
{
	char title[1000];
	int* value;
	int min;
	int max;
	int step;
};

class cMenu
{
private:

	int AddEntry(int n, char title[1000], int* value, int min, int max, int step);
	int menuSelect;
	int menuItems;
	menu_entrys menuEntry[1000];

public:

	void Init();
	void Draw();
	int KeyEvent(int keynum);
	int Active;
	int Key;

};

extern cMenu gMenu;

#endif